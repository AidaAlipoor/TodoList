﻿using System.Text;
using Microsoft.EntityFrameworkCore;
using TodoList.Business.Authentication.Exceptions;
using TodoList.Business.Authentication.ViewModels;
using TodoList.DataAccess;
using TodoList.DataAccess.User;
using TodoList.Util;

namespace TodoList.Business.Authentication;

public class AuthBusiness
{
    private readonly TodoDbContext _dbContext;
    private DbSet<UserEntity> UsersRepository => _dbContext.Set<UserEntity>();

    public AuthBusiness(TodoDbContext dbContext)
    {
        _dbContext = dbContext;
    }

    public async Task<int> RegisterAsync(RegisterDto dto)
    {
        var users = await UsersRepository.ToArrayAsync();
        var usernames = users.Select(u => u.Username).ToArray();

        Guard.Assert<DuplicateUsernameException>(usernames.Contains(dto.Username) is false);

        var entity = new UserEntity
        {
            Username = dto.Username,
            HashPassword = ToBase64(dto.Password),
            Email = dto.Email,
        };

        UsersRepository.Add(entity);

        await _dbContext.SaveChangesAsync();

        return entity.Id;
    }

    public async Task LoginAsync(string username, string password)
    {
        var entity = await UsersRepository.SingleOrDefaultAsync(u => u.Username == username);
        Guard.Assert<UsernameNotFoundException>(entity is not null);

        var hashedPassword = ToBase64(password);
        Guard.Assert<UsernameNotFoundException>(entity!.HashPassword == hashedPassword);
    }

    public async Task MailResetPasswordLinkAsync(string username)
    {
        throw new NotImplementedException();
    }

    public async Task<UserViewModel?> GetUserAsync(string username)
    {
        var entity = await UsersRepository.SingleOrDefaultAsync(u => u.Username == username);

        if (entity is null) return null;

        return new UserViewModel(entity.Id, entity.Username, entity.Email, entity.HashPassword);
    }


    private string ToBase64(string value) => Convert.ToBase64String(Encoding.UTF8.GetBytes(value));


    public record RegisterDto(string Username, string Password, string Email);
}