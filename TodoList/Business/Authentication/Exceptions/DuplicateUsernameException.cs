﻿namespace TodoList.Business.Authentication.Exceptions;

public class DuplicateUsernameException : Exception
{
    public DuplicateUsernameException() : base(message: "Username has already taken")
    {
    }
}