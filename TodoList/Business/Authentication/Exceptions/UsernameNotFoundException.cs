﻿namespace TodoList.Business.Authentication.Exceptions;

public class UsernameNotFoundException : Exception
{
    public UsernameNotFoundException() : base(message: "Username not found")
    {
    }
}