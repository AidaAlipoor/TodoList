﻿namespace TodoList.Business.Authentication.Exceptions;

public class WrongPasswordException : Exception
{
    public WrongPasswordException() : base(message: "Password is wrong")
    {
    }
}