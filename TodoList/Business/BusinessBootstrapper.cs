﻿using TodoList.Business.Authentication;
using TodoList.Business.TodoList;

namespace TodoList.Business;

public class BusinessBootstrapper
{
    public static void Run(IServiceCollection serviceCollection)
    {
        serviceCollection.AddScoped<TodoListBusiness>();
        serviceCollection.AddScoped<AuthBusiness>();
    }
}