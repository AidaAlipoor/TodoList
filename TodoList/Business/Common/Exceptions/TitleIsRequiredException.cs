﻿namespace TodoList.Business.Common.Exceptions;

public class TitleIsRequiredException : Exception
{
    public TitleIsRequiredException() : base(message: "Title is required")
    {
    }
}