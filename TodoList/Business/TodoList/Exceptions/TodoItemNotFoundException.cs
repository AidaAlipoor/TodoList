﻿namespace TodoList.Business.TodoList.Exceptions;

public class TodoItemNotFoundException : Exception
{
    public TodoItemNotFoundException() : base(message: "Todo item not found")
    {
    }
}