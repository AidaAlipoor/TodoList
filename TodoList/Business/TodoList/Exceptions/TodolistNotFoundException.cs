﻿namespace TodoList.Business.TodoList.Exceptions;

public class TodolistNotFoundException : Exception
{
    public TodolistNotFoundException() : base(message: "Todo list not found")
    {
    }
}