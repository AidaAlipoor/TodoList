﻿using Microsoft.EntityFrameworkCore;
using TodoList.Business.Common.Exceptions;
using TodoList.Business.TodoList.Exceptions;
using TodoList.DataAccess;
using TodoList.DataAccess.TodoItem;
using TodoList.DataAccess.TodoList;
using TodoList.Models;
using TodoList.RepositoryExtensions;
using TodoList.Util;

namespace TodoList.Business.TodoList;

public class TodoListBusiness
{
    private readonly TodoDbContext _dbContext;
    private DbSet<TodoListEntity> TodoListRepository => _dbContext.Set<TodoListEntity>();
    private DbSet<TodoItemEntity> TodoItemRepository => _dbContext.Set<TodoItemEntity>();

    public TodoListBusiness(TodoDbContext dbContext)
    {
        _dbContext = dbContext;
    }

    public async Task<int> CreateAsync(string title, int userId)
    {
        var entity = new TodoListEntity
        {
            Title = title,
            UserId = userId,
            UpdateDateTime = DateTime.Now,
        };

        TodoListRepository.Add(entity);

        await _dbContext.SaveChangesAsync();

        return entity.Id;
    }

    public async Task<TodoListViewModel[]> GetAsync()
    {
        var entities = await TodoListRepository.OrderByDescending(tl => tl.UpdateDateTime).ToArrayAsync();
        return entities.Select(tl => new TodoListViewModel(tl.Id, tl.Title)).ToArray();
    }

    public async Task<TodoListViewModel> GetAsync(int id)
    {
        var entity = await TodoListRepository.FindAsync(id);

        Guard.Assert<TodolistNotFoundException>(entity is not null);

        return new TodoListViewModel(entity.Id, entity.Title);
    }

    public async Task<TodoItemViewModel[]> GetTodoItemsAsync(int todoListId)
    {
        var entities = await TodoItemRepository.Where(ti => ti.TodoListId == todoListId)
            .ToArrayAsync();

        if (entities.Any() is false) return Array.Empty<TodoItemViewModel>();

        return entities.Select(ti => new TodoItemViewModel(ti.Id, ti.Title, ti.Description, ti.IsDone)).ToArray();
    }

    public async Task<TodoItemViewModel> GetTodoItemAsync(int todoItemId)
    {
        var entity = await TodoItemRepository.FindAsync(todoItemId);

        if (entity is null) throw new TodoItemNotFoundException();

        return new TodoItemViewModel(entity.Id, entity.Title, entity.Description, entity.IsDone);
    }

    public async Task AddTodoItemAsync(int id, TodoItemDto[] todoItemDtos)
    {
        var entity = await TodoListRepository.FindAsync(id);
        Guard.Assert<TodolistNotFoundException>(entity is not null);

        foreach (var dto in todoItemDtos)
        {
            Guard.Assert<TitleIsRequiredException>(string.IsNullOrWhiteSpace(dto.Title) is false);

            entity!.TodoItems.Add(new TodoItemEntity
            {
                Title = dto.Title,
                Description = dto.Description,
                UpdateDateTime = DateTime.Now
            });
        }

        TodoListRepository.Update(entity!);

        await _dbContext.SaveChangesAsync();
    }

    public async Task UpdateTodoItemAsync(int id, int todoItemId, TodoItemDto dto)
    {
        Guard.Assert<TitleIsRequiredException>(string.IsNullOrWhiteSpace(dto.Title) is false);

        var entity = await TodoListRepository.FindAsync(id);
        Guard.Assert<TodolistNotFoundException>(entity is not null);

        var todoItem = entity!.TodoItems.SingleOrDefault(ti => ti.Id == todoItemId);
        Guard.Assert<TodoItemNotFoundException>(todoItem is not null);

        todoItem.Title = dto.Title;
        todoItem.Description = dto.Description;

        TodoListRepository.Update(entity);

        await _dbContext.SaveChangesAsync();
    }

    public async Task DeleteTodoItemsAsync(int id, params int[] todoItemIds)
    {
        var entity = await TodoListRepository.FindAsync(id);
        Guard.Assert<TodolistNotFoundException>(entity is not null);

        TodoItemRepository.RemoveAll(ti => todoItemIds.Contains(ti.Id));

        await _dbContext.SaveChangesAsync();
    }

    public async Task UpdateAsync(int id, string title)
    {
        var entity = await TodoListRepository.FindAsync(id);
        Guard.Assert<TodolistNotFoundException>(entity is not null);

        entity!.Title = title;

        TodoListRepository.Update(entity);

        await _dbContext.SaveChangesAsync();
    }

    public async Task DeleteAsync(int id)
    {
        var entity = await TodoListRepository.Include(tl => tl.TodoItems)
            .SingleOrDefaultAsync(tl => tl.Id == id);
        Guard.Assert<TodolistNotFoundException>(entity is not null);

        TodoItemRepository.RemoveRange(entity!.TodoItems);
        TodoListRepository.Remove(entity!);

        await _dbContext.SaveChangesAsync();
    }

    public async Task MakeTodoItemDone(int todoListId, int todoItemId)
    {
        var todolist = await TodoListRepository.FindAsync(todoListId);
        Guard.Assert<TodolistNotFoundException>(todolist is not null);

        todolist!.MakeTodoItemDone(todoItemId);

        TodoListRepository.Update(todolist);

        await _dbContext.SaveChangesAsync();
    }

    public async Task MakeTodoItemUndone(int todoListId, int todoItemId)
    {
        var todolist = await TodoListRepository.FindAsync(todoListId);
        Guard.Assert<TodolistNotFoundException>(todolist is not null);

        todolist!.MakeTodoItemUndone(todoItemId);

        TodoListRepository.Update(todolist);

        await _dbContext.SaveChangesAsync();
    }
}

public record TodoItemDto(string Title, string? Description);