﻿using Microsoft.AspNetCore.Mvc;
using TodoList.Business.Authentication;

namespace TodoList.Controllers;

public class RegisterController : AbstractController
{
    private readonly AuthBusiness _authBusiness;

    public RegisterController(AuthBusiness authBusiness)
    {
        _authBusiness = authBusiness;
    }

    [HttpGet]
    public IActionResult Index()
    {
        if (HasUserAuthenticated())
            return RedirectToTodoListIndex();

        return View();
    }

    [HttpPost]
    public async Task<IActionResult> Register(string username, string password, string email)
    {
        await _authBusiness.RegisterAsync(new AuthBusiness.RegisterDto(username, password, email));
        return RedirectToTodoListIndex();
    }

    private IActionResult RedirectToTodoListIndex()
        => RedirectToAction(actionName: "Index", controllerName: "TodoList");
}