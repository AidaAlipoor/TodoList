﻿using Microsoft.AspNetCore.Mvc;
using TodoList.Business.TodoList;
using TodoList.Models;

namespace TodoList.Controllers;

public class TodoItemController : AbstractController
{
    private readonly TodoListBusiness _todoListBusiness;

    public TodoItemController(TodoListBusiness todoListBusiness)
    {
        _todoListBusiness = todoListBusiness;
    }

    [HttpGet]
    public async Task<IActionResult> Index(int todoListId)
    {
        if (HasUserAuthenticated() is false)
            return RedirectToLoginView();

        var todoList = await _todoListBusiness.GetAsync(todoListId);
        var todoItems = await _todoListBusiness.GetTodoItemsAsync(todoListId);

        return View(new TodoListWithItemsViewModel(todoList, todoItems));
    }

    [HttpGet]
    public async Task<IActionResult> GetTodoItem(int todoListId, int todoItemId)
    {
        if (HasUserAuthenticated() is false)
            return RedirectToLoginView();

        var todoList = await _todoListBusiness.GetAsync(todoListId);
        var todoItems = await _todoListBusiness.GetTodoItemsAsync(todoListId);
        var todoItem = await _todoListBusiness.GetTodoItemAsync(todoItemId);

        return View("Index", new TodoListWithItemsViewModel(todoList, todoItems, UpdatingTodoItem: todoItem));
    }

    [HttpPost]
    public async Task<IActionResult> Create(int todoListId, TodoItemDto dto)
    {
        await _todoListBusiness.AddTodoItemAsync(todoListId, new[] { dto });
        return RedirectToIndex(todoListId);
    }

    [HttpPost]
    public async Task<IActionResult> MakeItDone(int todoListId, int todoItemId)
    {
        await _todoListBusiness.MakeTodoItemDone(todoListId, todoItemId);

        return RedirectToIndex(todoListId);
    }

    [HttpPost]
    public async Task<IActionResult> MakeItUndone(int todoListId, int todoItemId)
    {
        await _todoListBusiness.MakeTodoItemUndone(todoListId, todoItemId);

        return RedirectToIndex(todoListId);
    }

    [HttpPost]
    public async Task<IActionResult> Update(int todoListId, int todoItemId, TodoItemDto dto)
    {
        await _todoListBusiness.UpdateTodoItemAsync(todoListId, todoItemId, dto);
        return RedirectToIndex(todoListId);
    }

    [HttpPost]
    public async Task<IActionResult> Delete(int todoListId, int todoItemId)
    {
        await _todoListBusiness.DeleteTodoItemsAsync(todoListId, todoItemId);
        return RedirectToIndex(todoListId);
    }

    private IActionResult RedirectToIndex(int todoListId) => RedirectToAction("Index", new { todoListId = todoListId });
}