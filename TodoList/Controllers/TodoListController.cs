﻿using Microsoft.AspNetCore.Mvc;
using TodoList.Business.TodoList;

namespace TodoList.Controllers;

public class TodoListController : AbstractController
{
    private readonly TodoListBusiness _todoListBusiness;

    public TodoListController(TodoListBusiness todoListBusiness)
    {
        _todoListBusiness = todoListBusiness;
    }

    [HttpGet]
    public async Task<IActionResult> Index()
    {
        if (HasUserAuthenticated() is false)
            return RedirectToLoginView();

        var viewModels = await _todoListBusiness.GetAsync();
        return View(viewModels);
    }

    [HttpPost]
    public async Task<IActionResult> Create(string title)
    {
        if (title.Length > 128)
        {
            TempData["ErrorMessage"] = "Title length must not exceed 128 characters.";
            return RedirectToAction(nameof(Index));
        }

        var userId = GetUserIdFromCookie()!;
        await _todoListBusiness.CreateAsync(title, userId.Value);

        return RedirectToIndex();
    }

    [HttpPost]
    public async Task<IActionResult> Delete(int id)
    {
        await _todoListBusiness.DeleteAsync(id);
        return RedirectToIndex();
    }


    private IActionResult RedirectToIndex() => RedirectToAction(nameof(Index));
}