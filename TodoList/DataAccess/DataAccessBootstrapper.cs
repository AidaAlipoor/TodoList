﻿using Microsoft.EntityFrameworkCore;

namespace TodoList.DataAccess;

public class DataAccessBootstrapper
{
    public static void Run(IServiceCollection serviceCollection, IConfiguration configuration)
    {
        serviceCollection.AddDbContext<TodoDbContext>(
            options => options.UseInMemoryDatabase($"TodoDb-{Guid.NewGuid()}"), ServiceLifetime.Singleton
        );
    }

    public static void EnsureDatabaseIsCreated(WebApplication app)
    {
        var scope = app.Services.CreateScope();
        var dbContext = scope.ServiceProvider.GetRequiredService<TodoDbContext>();
        dbContext.Database.EnsureCreated();
    }
}