﻿using Microsoft.EntityFrameworkCore;
using TodoList.DataAccess.TodoItem;
using TodoList.DataAccess.TodoList;
using TodoList.DataAccess.User;

namespace TodoList.DataAccess;

public class TodoDbContext : DbContext
{
    public TodoDbContext(DbContextOptions<TodoDbContext> options) : base(options)
    {
    }

    public DbSet<TodoItemEntity> TodoItems { get; set; }
    public DbSet<TodoListEntity> TodoLists { get; set; }
    public DbSet<UserEntity> Users { get; set; }

    protected override void OnModelCreating(ModelBuilder modelBuilder)
    {
        modelBuilder.ApplyConfigurationsFromAssembly(typeof(TodoItemConfiguration).Assembly);

        UserSeeder.Seed(modelBuilder);
        TodoListSeeder.Seed(modelBuilder);
        TodoItemSeeder.Seed(modelBuilder);
    }
}