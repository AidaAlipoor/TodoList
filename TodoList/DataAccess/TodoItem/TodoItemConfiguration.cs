﻿using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;

namespace TodoList.DataAccess.TodoItem;

public class TodoItemConfiguration : IEntityTypeConfiguration<TodoItemEntity>
{
    public void Configure(EntityTypeBuilder<TodoItemEntity> builder)
    {
        builder.HasKey(ti => ti.Id);
        builder.HasOne(ti => ti.TodoList).WithMany(tl => tl.TodoItems).HasForeignKey(ti => ti.TodoListId);
        builder.Property(ti => ti.Title).HasMaxLength(128).IsRequired();
        builder.Property(ti => ti.Description).HasMaxLength(1024);
    }
}