﻿using TodoList.DataAccess.TodoList;

namespace TodoList.DataAccess.TodoItem;

public class TodoItemEntity
{
    public int Id { get; set; }
    public int TodoListId { get; set; }
    public string Title { get; set; }
    public string? Description { get; set; }
    public bool IsDone { get; set; }
    public DateTime UpdateDateTime { get; set; }

    public TodoListEntity TodoList { get; set; }
}