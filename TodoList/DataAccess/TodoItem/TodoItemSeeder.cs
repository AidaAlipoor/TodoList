﻿using Microsoft.EntityFrameworkCore;

namespace TodoList.DataAccess.TodoItem;

public class TodoItemSeeder
{
    public static void Seed(ModelBuilder modelBuilder)
    {
        modelBuilder.Entity<TodoItemEntity>().HasData(new[]
        {
            new TodoItemEntity
            {
                Id = 1,
                TodoListId = 1,
                Title = "Do laundry",
                Description = "Don't forget the hats",
                UpdateDateTime = DateTime.Now,
                IsDone = false
            },
            new TodoItemEntity
            {
                Id = 2,
                TodoListId = 1,
                Title = "Call Mom",
                Description = null,
                UpdateDateTime = DateTime.Now.AddHours(-6),
                IsDone = true
            },
            new TodoItemEntity
            {
                Id = 3,
                TodoListId = 1,
                Title = "Lorem ipsum",
                Description = "Lorem ipsum dolor sit amet, consectetuer adipiscing elit. " +
                              "Aenean commodo ligula eget dolor. Aenean massa. " +
                              "Cum sociis natoque penatibus et magnis dis parturient montes, nascetur ridiculus mus. " +
                              "Donec qu",
                UpdateDateTime = DateTime.Now.AddHours(-10),
                IsDone = true
            },
        });
    }
}