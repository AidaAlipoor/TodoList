﻿using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;

namespace TodoList.DataAccess.TodoList;

public class TodoListConfiguration : IEntityTypeConfiguration<TodoListEntity>
{
    public void Configure(EntityTypeBuilder<TodoListEntity> builder)
    {
        builder.HasKey(tl => tl.Id);
        builder.HasOne(tl => tl.User).WithMany(u => u.TodoLists).HasForeignKey(tl => tl.UserId).IsRequired();
        builder.Property(tl => tl.Title).HasMaxLength(128).IsRequired();
    }
}