﻿using TodoList.Business.TodoList.Exceptions;
using TodoList.DataAccess.TodoItem;
using TodoList.DataAccess.User;
using TodoList.Util;

namespace TodoList.DataAccess.TodoList;

public class TodoListEntity
{
    public int Id { get; set; }
    public int UserId { get; set; }
    public string Title { get; set; }
    public DateTime UpdateDateTime { get; set; }

    public UserEntity User { get; set; }
    public List<TodoItemEntity> TodoItems { get; set; }

    public void MakeTodoItemDone(int id)
    {
        var todoItem = TodoItems.SingleOrDefault(ti => ti.Id == id);
        Guard.Assert<TodoItemNotFoundException>(todoItem is not null);

        todoItem!.IsDone = true;
    }

    public void MakeTodoItemUndone(int id)
    {
        var todoItem = TodoItems.SingleOrDefault(ti => ti.Id == id);
        Guard.Assert<TodoItemNotFoundException>(todoItem is not null);

        todoItem!.IsDone = false;
    }
}