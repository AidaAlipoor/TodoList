﻿using Microsoft.EntityFrameworkCore;

namespace TodoList.DataAccess.TodoList;

public class TodoListSeeder
{
    public static void Seed(ModelBuilder modelBuilder)
    {
        modelBuilder.Entity<TodoListEntity>().HasData(new[]
        {
            new TodoListEntity
            {
                Id = 1,
                UserId = 1,
                Title = "Today's Tasks",
                UpdateDateTime = DateTime.Now
            },
            new TodoListEntity
            {
                Id = 2,
                UserId = 1,
                Title = "Things to do for office",
                UpdateDateTime = DateTime.Now.AddDays(-1).AddHours(-5)
            }
        });
    }
}