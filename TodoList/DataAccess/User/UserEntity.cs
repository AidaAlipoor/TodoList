﻿using TodoList.DataAccess.TodoList;

namespace TodoList.DataAccess.User;

public class UserEntity
{
    public int Id { get; set; }
    public string Username { get; set; }
    public string HashPassword { get; set; }
    public string Email { get; set; }
    
    public List<TodoListEntity> TodoLists { get; set; }
}