﻿using System.Text;
using Microsoft.EntityFrameworkCore;

namespace TodoList.DataAccess.User;

public class UserSeeder
{
    public static void Seed(ModelBuilder modelBuilder)
    {
        var adminBase64Password = Convert.ToBase64String(Encoding.UTF8.GetBytes("admin"));
        
        modelBuilder.Entity<UserEntity>().HasData(new[]
        {
            new UserEntity
            {
                Id = 1,
                Username = "admin",
                HashPassword = adminBase64Password,
                Email = "admin@email.com"
            }
        });
    }
}