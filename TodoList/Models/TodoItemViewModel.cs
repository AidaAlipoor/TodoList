﻿namespace TodoList.Models;

public record TodoItemViewModel(int Id, string Title, string? Description, bool IsDone);