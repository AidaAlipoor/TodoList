﻿namespace TodoList.Models;

public record TodoListViewModel(int Id, string Title);