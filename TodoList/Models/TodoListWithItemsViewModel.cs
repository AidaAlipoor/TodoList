﻿namespace TodoList.Models;

public record TodoListWithItemsViewModel(
    TodoListViewModel TodoList,
    TodoItemViewModel[] TodoItems,
    TodoItemViewModel? UpdatingTodoItem = null);