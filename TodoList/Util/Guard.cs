﻿namespace TodoList.Util;

public class Guard
{
    public static void Assert<TException>(bool isValid) where TException : Exception, new()
    {
        if (isValid is false) throw new TException();
    }
}